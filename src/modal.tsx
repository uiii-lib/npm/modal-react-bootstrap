import React, { HTMLAttributes, PropsWithChildren } from 'react';
import { Modal as RBModal } from 'react-bootstrap';

export interface ModalProps extends HTMLAttributes<HTMLDivElement> {
	title?: string;
	isOpened: boolean;
	close: () => void;
}

export const Modal = (props: PropsWithChildren<ModalProps>) => {
	const {title, isOpened, close, children, ...rest} = props;

	return (
		<RBModal {...rest} show={isOpened} onHide={close}>
			{title && <RBModal.Header closeButton>{title}</RBModal.Header>}
			<RBModal.Body>{children}</RBModal.Body>
		</RBModal>
	)
}
